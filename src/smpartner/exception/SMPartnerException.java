package smpartner.exception;

import java.lang.invoke.MethodHandles;

public class SMPartnerException extends Exception {

	private static final long serialVersionUID = 651108797057643815L;

	protected static String CLASS_NAME = MethodHandles.lookup().lookupClass().toString();

	public SMPartnerException() {
		super();
	}

	public SMPartnerException(String message) {
		super(message);
	}

	public SMPartnerException(Throwable cause) {
		super(cause);
	}

	public SMPartnerException(String message, Throwable cause) {
		super(message, cause);
	}

}