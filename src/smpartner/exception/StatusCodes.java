package smpartner.exception;

public class StatusCodes {

	public static interface AppStatusCodes {
		String genericSuccessCode = "2000";
		String genericClientErrorCode = "4000";
		String genericSecurityErrorCode = "4010";
		String genericServerErrorCode = "5000";

	}

	public static interface ClientErrors {
		String badRequest = "4000"; // The server cannot or will not process the request due to invalid request.
		String unauthorized = "4001"; // Authentication is required and has failed or has not yet been provided.
		String forbidden = "4003"; // The request was valid, but the server is refusing action.
		String timeout = "4008";

	}

	public static interface ClientErrorMessage {
		String badRequest = "The server cannot or will not process the request due to invalid request ";
		String unauthorized = "Authentication is required and has failed or has not yet been provided ";
		String forbidden = "The request was valid, but the server is refusing action";

	}

	public static interface ServerErrors {
		String internalServerError = "5000";

	}

	public static interface AppModuleMsg {
		String PO_SUCCESS = "Purchase Order saved successfully";
		String PO_ERROR_MSG = "Server encountered an unexpected condition while on create 'Purchase Order' request.";
		String PO_OTB_ERROR_MSG = "Server encountered an unexpected condition while on read 'Purchase Order OTB' request.";

		String PO_HSN_ERROR_MSG = "Server encountered an unexpected condition while on read 'Department HSN code' request.";
		
		String PO_ITEM_BARCODE_MSG = "Server encountered an unexpected condition while on read 'Item Barcode' request.";

	}

	public static interface AppStatusMsg {
		String BLANK = "No Data Found";
		String MISMATCH = "Found Mismatch Item Combination";
		String NUMBER_FORMAT_MSG = "Unable to process your request, Please try with valid ID";
		String FAILURE_MSG = "Unable to process your request, Please check error for more information";
		String INVALID_JSON = "Invalid JSON structure. Please check the request payload";
		String BLANK_JSON = "JSON is empty";
		String SUCCESS_MSG = "Request completed successfully";
		String NETWORK_ERROR = "Network is not availabe, Please check network connection";

	}
	
	public static interface AppModuleCode{
		String POCREATORERROR = "PROPORXXX1";
		String POOTB_READER_ERROR = "PROPOROTB2";
		String PO_HSN_READER_ERROR = "PROPORHSN2";
		String PO_ITEM_BARCODE_ERROR = "PROPORITM2";
	}
}