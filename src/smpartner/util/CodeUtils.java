package smpartner.util;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public interface CodeUtils {

	String RESPONSE = "resource";
	String RESPONSE_MSG = "message";
	final String PROPERTIES_PATH = "resources/application.properties";

	static <T> boolean isEmpty(T data) {

		if (data == null) {
			return true;
		} else if (data instanceof String) {
			return data.toString().isEmpty();
		} else if (data instanceof List) {
			return ((List) data).isEmpty();
		} else if (data instanceof Set) {
			return ((Set) data).isEmpty();
		} else if (data instanceof Map) {
			return ((Map) data).isEmpty();
		}
		return false;

	}

	static Properties setAllPropertiesAccordingEnterprise(String entPrefix) throws IOException {

		Properties ginesysProperties = ConnectionUtils.getProperties(PROPERTIES_PATH);

		ginesysProperties.setProperty("database", ginesysProperties.getProperty(entPrefix + ".database"));
		ginesysProperties.setProperty("tablename", ginesysProperties.getProperty(entPrefix + ".tablename"));
		ginesysProperties.setProperty("url", ginesysProperties.getProperty(entPrefix + ".url"));
		ginesysProperties.setProperty("driverclass", ginesysProperties.getProperty(entPrefix + ".driverclass"));
		ginesysProperties.setProperty("username", ginesysProperties.getProperty(entPrefix + ".username"));
		ginesysProperties.setProperty("password", ginesysProperties.getProperty(entPrefix + ".password"));

		return ginesysProperties;
	}
}
