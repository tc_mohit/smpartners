package smpartner.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import smpartner.layer.data.GinesysPurchaseOrder;

public class DBUtilsQA {

	public static int insertPurchaseOrder(GinesysPurchaseOrder po)
			throws SQLException, ClassNotFoundException, IOException {

		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("databaseqa");
		String tableName = ginesysProperties.getProperty("tablenameqa");
		String userName = ginesysProperties.getProperty("usernameqa");

		PreparedStatement pstm = getPurchaseOrderStatement(po, database, tableName, ginesysProperties);
		int result = pstm.executeUpdate();
//		System.out.println("Result : " + result);
		return result;
	}

	public static int insertSBazaarPO(GinesysPurchaseOrder po)
			throws SQLException, ClassNotFoundException, IOException {

		Properties ginesysProperties = ConnectionUtils.getProperties("resources/sbazaar.properties");
		String database = ginesysProperties.getProperty("databaseqa");
		String tableName = ginesysProperties.getProperty("tablenameqa");
		String userName = ginesysProperties.getProperty("usernameqa");

		PreparedStatement pstm = getPurchaseOrderStatement(po, database, tableName, ginesysProperties);
		int result = pstm.executeUpdate();
//		System.out.println("Result : " + result);
		return result;
	}

	public static PreparedStatement getPurchaseOrderStatement(GinesysPurchaseOrder po, String database,
			String tableName, Properties prop) throws SQLException, ClassNotFoundException, IOException {
		String sql = "INSERT INTO " + database + "." + tableName + "(INTGCODE, INTG_HEADER_ID, INTG_LINE_ID,"
				+ "				ORDER_NO, ORDER_DATE, VENDOR_ID, TRANSPORTER_ID, AGENT_ID, AGENT_RATE,"
				+ "				PO_REMARKS, CREATED_BY_ID, VALID_FROM, VALID_TO, MERCHANDISER_ID,"
				+ "				SITE_ID, ITEM_ID, SET_REMARKS, SET_RATIO, ARTICLE_ID, ITEM_NAME,"
				+ "				CCODE1, CCODE2, CCODE3, CCODE4, CCODE5, CCODE6, CNAME1, CNAME2,"
				+ "				CNAME3, CNAME4, CNAME5, CNAME6, DESC1, DESC2, DESC3, DESC4, DESC5,"
				+ "				DESC6, MRP, LISTED_MRP, WSP, UOM, MATERIAL_TYPE, QTY, RATE,"
				+ "				PO_ITEM_REMARKS, ISIMPORTED, TERMCODE, ISVALIDATED, VALIDATION_ERROR,"
				+ "				DOCCODE, SET_HEADER_ID, KEY, POUDFSTRIN01, POUDFSTRIN02, POUDFSTRIN03,"
				+ "				POUDFSTRIN04, POUDFSTRIN05, POUDFSTRIN06, POUDFSTRIN07, POUDFSTRIN08,"
				+ "				POUDFSTRIN09, POUDFSTRIN010, POUDFNUM01, POUDFNUM02, POUDFNUM03,"
				+ "				POUDFNUM04, POUDFNUM05, POUDFDATE01, POUDFDATE02, POUDFDATE03,"
				+ "				POUDFDATE04, POUDFDATE05, SMUDFSTRIN01, SMUDFSTRIN02, SMUDFSTRIN03,"
				+ "				SMUDFSTRIN04, SMUDFSTRIN05, SMUDFSTRIN06, SMUDFSTRIN07, SMUDFSTRIN08,"
				+ "				SMUDFSTRIN09, SMUDFSTRIN010, SMUDFNUM01, SMUDFNUM02, SMUDFNUM03,"
				+ "				SMUDFNUM04, SMUDFNUM05, SMUDFDATE01, SMUDFDATE02, SMUDFDATE03,"
				+ "				SMUDFDATE04, SMUDFDATE05, IMUDFSTRING01, IMUDFSTRING02, IMUDFSTRING03,"
				+ "				IMUDFSTRING04, IMUDFSTRING05, IMUDFSTRING06, IMUDFSTRING07,"
				+ "				IMUDFSTRING08, IMUDFSTRING09, IMUDFSTRING10, IMUDFNUM01, IMUDFNUM02,"
				+ "				IMUDFNUM03, IMUDFNUM04, IMUDFNUM05, IMUDFDATE01, IMUDFDATE02,"
				+ "				IMUDFDATE03, IMUDFDATE04, IMUDFDATE05,PO_REFERENCE_ID, HSN_CODE ) VALUES "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		Connection con = ConnectionUtils.getConnection(prop);
		PreparedStatement pstm = con.prepareStatement(sql);
		pstm.setString(1, po.getIntGCode());
		pstm.setString(2, po.getIntGHeaderId());
		pstm.setString(3, po.getIntGLineId());
		pstm.setString(4, po.getOrderNo());
		pstm.setTimestamp(5, po.getOrderDate());
		pstm.setString(6, po.getVendorId());
		pstm.setString(7, po.getTransporterId());
		pstm.setString(8, po.getAgentId());
		pstm.setDouble(9, po.getAgentRate());
		pstm.setString(10, po.getPoRemarks());
		pstm.setString(11, po.getCreatedById());
		pstm.setTimestamp(12, po.getValidFrom());
//		System.out.println("Valid from :" + po.getValidFrom());
		pstm.setTimestamp(13, po.getValidTo());
		pstm.setString(14, po.getMerchandiserId());
		pstm.setString(15, po.getSiteId());
		pstm.setString(16, po.getItemId());
		pstm.setString(17, po.getSetRemarks());
		pstm.setDouble(18, po.getSetRatio());
		pstm.setString(19, po.getArticleId());
		pstm.setString(20, po.getItemName());
		pstm.setString(21, po.getcCode1());
		pstm.setString(22, po.getcCode2());
		pstm.setString(23, po.getcCode3());
		pstm.setString(24, po.getcCode4());
		pstm.setString(25, po.getcCode5());
		pstm.setString(26, po.getcCode6());
		pstm.setString(27, po.getcName1());
		pstm.setString(28, po.getcName2());
		pstm.setString(29, po.getcName3());
		pstm.setString(30, po.getcName4());
		pstm.setString(31, po.getcName5());
		pstm.setString(32, po.getcName6());
		pstm.setString(33, po.getDesc1());
		pstm.setString(34, po.getDesc2());
		pstm.setString(35, po.getDesc3());
		pstm.setString(36, po.getDesc4());
		pstm.setString(37, po.getDesc5());
		pstm.setString(38, po.getDesc6());
		pstm.setDouble(39, po.getMrp());
		pstm.setDouble(40, po.getListedMRP());
		pstm.setDouble(41, po.getWsp());
		pstm.setString(42, po.getUom());
		pstm.setString(43, String.valueOf(po.getMaterialType()));
		pstm.setDouble(44, po.getQty());
		pstm.setDouble(45, po.getRate());
		pstm.setString(46, po.getPoItemRemarks());
		pstm.setString(47, String.valueOf(po.getIsImported()));
		pstm.setString(48, po.getTermCode());
		pstm.setString(49, String.valueOf(po.getIsValidated()));
		pstm.setString(50, po.getValidationError());
		pstm.setString(51, po.getDocCode());
		pstm.setString(52, po.getSetHeaderId());
		pstm.setInt(53, po.getKey());
		pstm.setString(54, po.getPoudfStrin01());
		pstm.setString(55, po.getPoudfStrin02());
		pstm.setString(56, po.getPoudfStrin03());
		pstm.setString(57, po.getPoudfStrin04());
		pstm.setString(58, po.getPoudfStrin05());
		pstm.setString(59, po.getPoudfStrin06());
		pstm.setString(60, po.getPoudfStrin07());
		pstm.setString(61, po.getPoudfStrin08());
		pstm.setString(62, po.getPoudfStrin09());
		pstm.setString(63, po.getPoudfStrin010());
		pstm.setString(64, po.getPoudfNum01());
		pstm.setString(65, po.getPoudfNum02());
		pstm.setString(66, po.getPoudfNum03());
		pstm.setString(67, po.getPoudfNum04());
		pstm.setString(68, po.getPoudfNum05());
		pstm.setTimestamp(69, po.getPoudfDate01());
		pstm.setTimestamp(70, po.getPoudfDate02());
		pstm.setTimestamp(71, po.getPoudfDate03());
		pstm.setTimestamp(72, po.getPoudfDate04());
		pstm.setTimestamp(73, po.getPoudfDate05());
		pstm.setString(74, po.getSmUDFStrin01());
		pstm.setString(75, po.getSmUDFStrin02());
		pstm.setString(76, po.getSmUDFStrin03());
		pstm.setString(77, po.getSmUDFStrin04());
		pstm.setString(78, po.getSmUDFStrin05());
		pstm.setString(79, po.getSmUDFStrin06());
		pstm.setString(80, po.getSmUDFStrin07());
		pstm.setString(81, po.getSmUDFStrin08());
		pstm.setString(82, po.getSmUDFStrin09());
		pstm.setString(83, po.getSmUDFStrin010());
		pstm.setString(84, po.getSmUDFNum01());
		pstm.setString(85, po.getSmUDFNum02());
		pstm.setString(86, po.getSmUDFNum03());
		pstm.setString(87, po.getSmUDFNum04());
		pstm.setString(88, po.getSmUDFNum05());
		pstm.setTimestamp(89, po.getSmUDFDate01());
		pstm.setTimestamp(90, po.getSmUDFDate02());
		pstm.setTimestamp(91, po.getSmUDFDate03());
		pstm.setTimestamp(92, po.getSmUDFDate04());
		pstm.setTimestamp(93, po.getSmUDFDate05());
		pstm.setString(94, po.getImUDFString01());
		pstm.setString(95, po.getImUDFString02());
		pstm.setString(96, po.getImUDFString03());
		pstm.setString(97, po.getImUDFString04());
		pstm.setString(98, po.getImUDFString05());
		pstm.setString(99, po.getImUDFString06());
		pstm.setString(100, po.getImUDFString07());
		pstm.setString(101, po.getImUDFString08());
		pstm.setString(102, po.getImUDFString09());
		pstm.setString(103, po.getImUDFString010());
		pstm.setString(104, po.getImUDFNum01());
		pstm.setString(105, po.getImUDFNum02());
		pstm.setString(106, po.getImUDFNum03());
		pstm.setString(107, po.getImUDFNum04());
		pstm.setString(108, po.getImUDFNum05());
		pstm.setTimestamp(109, po.getImUDFDate01());
		pstm.setTimestamp(110, po.getImUDFDate02());
		pstm.setTimestamp(111, po.getImUDFDate03());
		pstm.setTimestamp(112, po.getImUDFDate04());
		pstm.setTimestamp(113, po.getImUDFDate05());
		pstm.setString(114, po.getPoReferenceId());
		pstm.setString(115, po.getHsnCode());

		return pstm;
	}

	public static Map<String, String> getOTB(Map<String, String> otbParameters)
			throws SQLException, ClassNotFoundException, IOException {

//		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
//		String database = ginesysProperties.getProperty("database");
//		
//		
//		String runSP = "{ call "+database+".get_udf_setting_by_code(?,?) }";
//
//		Connection con = ConnectionUtils.getConnection();
//
//		CallableStatement callableStatement = con.prepareCall(runSP);
//
//		callableStatement.setInt(1, 21);
//
//		callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
//
//		// run it
//		callableStatement.executeUpdate();
//
//		String displayName = callableStatement.getString(2);
//
//		return displayName;

		Map<String, String> otbMap = null;

		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("database");

		Connection con = ConnectionUtils.getConnection(ginesysProperties);

		Statement stmt = con.createStatement();
		otbMap = new HashMap<String, String>();
		String query = "select balance from " + database + ".BALANCE_OTB where invarticle_code = '"
				+ otbParameters.get("articleCode") + "' " + "and desc6  = '" + otbParameters.get("desc6")
				+ "' and month_year = '" + otbParameters.get("monthYear") + "'";
		ResultSet rs = stmt.executeQuery(query);
		if (rs != null) {
			while (rs.next()) {
				otbMap.put("otb", rs.getString("balance"));
			}
		} else {
			otbMap.put("otb", "0");

		}

		rs.close();
		con.close();

		return otbMap;

	}

	public static Map<String, String> getHsnByHl3Code(String hl3Code)
			throws SQLException, ClassNotFoundException, IOException {

		Map<String, String> hsnMap = new HashMap<String, String>();

		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("database");

		Connection con = ConnectionUtils.getConnection(ginesysProperties);

		Statement stmt = con.createStatement();

		ResultSet rs = stmt.executeQuery("select invhsnsacmain_code, hsn_sac_code  from " + database
				+ ".item_group where department_code = " + hl3Code + " and rownum = 1");
		while (rs.next()) {

			hsnMap.put("hsnCode", rs.getString("invhsnsacmain_code"));
			hsnMap.put("hsnSacCode", rs.getString("hsn_sac_code"));
		}
		rs.close();
		con.close();

		return hsnMap;

	}

	public static List<Map<String, String>> getItemBarcode(Map<String, String> itemParameters)
			throws SQLException, ClassNotFoundException, IOException {

		Map<String, String> itemMap = null;
		Map<String, String> pageMap = null;
		List<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
		ResultSet rs = null;
		int previousPage = 0;
		int pageSize = 10;
		int totalRecord = 0;
		int offset = 0;
		int maxPage = 0;
		int pageNo = Integer.parseInt(itemParameters.get("pageNo"));
		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("database");

		Connection con = ConnectionUtils.getConnection(ginesysProperties);

		Statement stmt = con.createStatement();

		ResultSet countRS = stmt.executeQuery(getItemBarcodeQuery(itemParameters, true, database));

		if (countRS != null) {
			if (countRS.next()) {
				totalRecord = countRS.getInt("COUNT");

				maxPage = (totalRecord + pageSize - 1) / pageSize;
				previousPage = pageNo - 1;
				offset = (pageNo == 1) ? pageNo - 1 : (pageNo - 1) * pageSize;

				itemParameters.put("offset", "" + offset);
				itemParameters.put("pageSize", "" + pageSize);

				pageMap = new HashMap<String, String>();
				pageMap.put("currPage", "" + pageNo);
				pageMap.put("prePage", "" + previousPage);
				pageMap.put("maxPage", "" + maxPage);

				rs = stmt.executeQuery(getItemBarcodeQuery(itemParameters, false, database));
				if (rs != null) {
					itemList.add(pageMap);
					while (rs.next()) {
						itemMap = new HashMap<String, String>();
						itemMap.put("item_id", rs.getString("item_id"));
						itemList.add(itemMap);
					}
				} else {
					itemList = null;
				}
			}
		} else {
			itemList = null;
		}

		rs.close();
		countRS.close();

		con.close();

		return itemList;
	}

	public static String getItemBarcodeQuery(Map<String, String> itemParameters, boolean isCount, String database) {
		String query = "";

		if (itemParameters.get("type").equals("1") && isCount == true) {

			query = "select count(*) count from ( " + "select distinct item_id from " + database + ".gin_purord where "
					+ "vendor_id = '" + itemParameters.get("supplierCode") + "' and" + " article_id = '"
					+ itemParameters.get("articleCode") + "' and " + "site_id = '" + itemParameters.get("siteId")
					+ "')";

		} else if (itemParameters.get("type").equals("1") && isCount == false) {
			query = "select distinct item_id from " + database + ".gin_purord where " + "vendor_id = '"
					+ itemParameters.get("supplierCode") + "' and" + " article_id = '"
					+ itemParameters.get("articleCode") + "' and " + "site_id = '" + itemParameters.get("siteId") + "'"
					+ " OFFSET '" + itemParameters.get("offset") + "' ROWS FETCH NEXT '"
					+ itemParameters.get("pageSize") + "' ROWS ONLY";

		} else if (itemParameters.get("type").equals("3") && isCount == true)
			query = "select count(*) count from (select distinct  item_id from " + database + ".gin_purord where "
					+ "vendor_id = '" + itemParameters.get("supplierCode") + "' and" + " article_id = '"
					+ itemParameters.get("articleCode") + "' and " + "site_id = '" + itemParameters.get("siteId") + "'"
					+ " and item_id like '%" + itemParameters.get("search") + "%' )";

		else if (itemParameters.get("type").equals("3") && isCount == false) {
			query = "select distinct  item_id from " + database + ".gin_purord where " + "vendor_id = '"
					+ itemParameters.get("supplierCode") + "' and" + " article_id = '"
					+ itemParameters.get("articleCode") + "' and " + "site_id = '" + itemParameters.get("siteId") + "'"
					+ " and item_id like '%" + itemParameters.get("search") + "%' " + " OFFSET '"
					+ itemParameters.get("offset") + "' ROWS FETCH NEXT '" + itemParameters.get("pageSize")
					+ "' ROWS ONLY";

		}
//		System.out.println("query :"+query);
		return query;
	}

	public static ObjectNode getItemBarcodeDetail(JsonNode data)
			throws SQLException, ClassNotFoundException, IOException {

		ResultSet itemDetailRS = null;
		ResultSet catdescudfrs = null;
		ResultSet otbRS = null;
		ObjectMapper mapper = new ObjectMapper();

		ObjectNode catDescNode = null;
		ObjectNode itemsNode = null;
		ArrayNode itemDetailNode = mapper.createArrayNode();
		ObjectNode items = mapper.createObjectNode();

		String itemsString = ((ArrayNode) data.get("items")).toString();
		String itemParameters[] = itemsString.substring(1, itemsString.length() - 1).split(",");
		String catDescParameter = itemsString.substring(1, itemsString.length() - 1).replace("\"", "'");;

		String catdescquery = "";
		String itemListQuery = "";
		String otbQuery = "";
		String articleCode = data.get("articleCode").asText();
		String desc6 = "";
		String monthYear = data.get("validTo").asText().substring(3).toUpperCase();
		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("database");
		Connection con = ConnectionUtils.getConnection(ginesysProperties);
		Statement stmt = con.createStatement();

		catdescquery = "select distinct mrp, mrp rsp, rate, ccode1, ccode2, ccode3, ccode4, cname1, cname2, cname3, cname4, desc1, desc2, desc3, desc4, desc5, desc6, imudfstring01, imudfstring02, imudfstring03, \n"
				+ " imudfstring04, imudfstring05, imudfstring06, imudfstring07, imudfstring08, imudfstring09, imudfstring10,\n"
				+ " imudfnum01, imudfnum02, imudfnum03, imudfnum04, imudfnum05, imudfdate01, imudfdate02, imudfdate03, imudfdate04, imudfdate05 from "
				+ database + ".GIN_PURORD where  item_id in (" + catDescParameter + ") and rownum = 1";

//			System.out.println("CATDESCQUERY :"+catdescquery);
//			System.out.println("itemListQuery :"+itemListQuery);
		catdescudfrs = stmt.executeQuery(catdescquery);
		if (catdescudfrs != null) {

			// (po.getPoudfDate01() != null) ? po.getPoudfDate01().toString() : null

			if (catdescudfrs.next()) {
				catDescNode = mapper.createObjectNode();
				catDescNode.put("mrp", (catdescudfrs.getString("mrp") != null) ? catdescudfrs.getString("mrp") : "");
				catDescNode.put("rsp", (catdescudfrs.getString("rsp") != null) ? catdescudfrs.getString("rsp") : "");
				catDescNode.put("rate", (catdescudfrs.getString("rate") != null) ? catdescudfrs.getString("rate") : "");
				catDescNode.put("cat1Code",
						(catdescudfrs.getString("ccode1") != null) ? catdescudfrs.getString("ccode1") : "");
				catDescNode.put("cat1Name",
						(catdescudfrs.getString("cname1") != null) ? catdescudfrs.getString("cname1") : "");
				catDescNode.put("cat2Code",
						(catdescudfrs.getString("ccode2") != null) ? catdescudfrs.getString("ccode2") : "");
				catDescNode.put("cat2Name",
						(catdescudfrs.getString("cname2") != null) ? catdescudfrs.getString("cname2") : "");
				catDescNode.put("cat3Code",
						(catdescudfrs.getString("ccode3") != null) ? catdescudfrs.getString("ccode3") : "");
				catDescNode.put("cat3Name",
						(catdescudfrs.getString("cname3") != null) ? catdescudfrs.getString("cname3") : "");
				catDescNode.put("cat4Code",
						(catdescudfrs.getString("ccode4") != null) ? catdescudfrs.getString("ccode4") : "");
				catDescNode.put("cat4Name",
						(catdescudfrs.getString("cname4") != null) ? catdescudfrs.getString("cname4") : "");
				catDescNode.put("desc1Code", "");
				catDescNode.put("desc2Code", "");
				catDescNode.put("desc3Code", "");
				catDescNode.put("desc4Code", "");
				catDescNode.put("desc5Code", "");
				catDescNode.put("desc6Code", "");
				catDescNode.put("desc1Name",
						(catdescudfrs.getString("desc1") != null) ? catdescudfrs.getString("desc1") : "");
				catDescNode.put("desc2Name",
						(catdescudfrs.getString("desc2") != null) ? catdescudfrs.getString("desc2") : "");
				catDescNode.put("desc3Name",
						(catdescudfrs.getString("desc3") != null) ? catdescudfrs.getString("desc3") : "");
				catDescNode.put("desc4Name",
						(catdescudfrs.getString("desc4") != null) ? catdescudfrs.getString("desc4") : "");
				catDescNode.put("desc5Name",
						(catdescudfrs.getString("desc5") != null) ? catdescudfrs.getString("desc5") : "");
				catDescNode.put("desc6Name",
						(catdescudfrs.getString("desc6") != null) ? catdescudfrs.getString("desc6") : "");
				catDescNode.put("itemudf1",
						(catdescudfrs.getString("imudfstring01") != null) ? catdescudfrs.getString("imudfstring01")
								: "");
				catDescNode.put("itemudf2",
						(catdescudfrs.getString("imudfstring02") != null) ? catdescudfrs.getString("imudfstring02")
								: "");
				catDescNode.put("itemudf3",
						(catdescudfrs.getString("imudfstring03") != null) ? catdescudfrs.getString("imudfstring03")
								: "");
				catDescNode.put("itemudf4",
						(catdescudfrs.getString("imudfstring04") != null) ? catdescudfrs.getString("imudfstring04")
								: "");
				catDescNode.put("itemudf5",
						(catdescudfrs.getString("imudfstring05") != null) ? catdescudfrs.getString("imudfstring05")
								: "");
				catDescNode.put("itemudf6",
						(catdescudfrs.getString("imudfstring06") != null) ? catdescudfrs.getString("imudfstring06")
								: "");
				catDescNode.put("itemudf7",
						(catdescudfrs.getString("imudfstring07") != null) ? catdescudfrs.getString("imudfstring07")
								: "");
				catDescNode.put("itemudf8",
						(catdescudfrs.getString("imudfstring08") != null) ? catdescudfrs.getString("imudfstring08")
								: "");
				catDescNode.put("itemudf9",
						(catdescudfrs.getString("imudfstring09") != null) ? catdescudfrs.getString("imudfstring09")
								: "");
				catDescNode.put("itemudf10",
						(catdescudfrs.getString("imudfstring10") != null) ? catdescudfrs.getString("imudfstring10")
								: "");
				catDescNode.put("itemudf11",
						(catdescudfrs.getString("imudfnum01") != null) ? catdescudfrs.getString("imudfnum01") : "");
				catDescNode.put("itemudf12",
						(catdescudfrs.getString("imudfnum02") != null) ? catdescudfrs.getString("imudfnum02") : "");
				catDescNode.put("itemudf13",
						(catdescudfrs.getString("imudfnum03") != null) ? catdescudfrs.getString("imudfnum03") : "");
				catDescNode.put("itemudf14",
						(catdescudfrs.getString("imudfnum04") != null) ? catdescudfrs.getString("imudfnum04") : "");
				catDescNode.put("itemudf15",
						(catdescudfrs.getString("imudfnum05") != null) ? catdescudfrs.getString("imudfnum05") : "");
				catDescNode.put("itemudf16",
						(catdescudfrs.getString("imudfdate01") != null) ? catdescudfrs.getString("imudfdate01") : "");
				catDescNode.put("itemudf17",
						(catdescudfrs.getString("imudfdate02") != null) ? catdescudfrs.getString("imudfdate02") : "");
				catDescNode.put("itemudf18",
						(catdescudfrs.getString("imudfdate03") != null) ? catdescudfrs.getString("imudfdate03") : "");
				catDescNode.put("itemudf19",
						(catdescudfrs.getString("imudfdate04") != null) ? catdescudfrs.getString("imudfdate04") : "");
				catDescNode.put("itemudf20",
						(catdescudfrs.getString("imudfdate05") != null) ? catdescudfrs.getString("imudfdate05") : "");

				catdescudfrs.close();
			}

			desc6 = catDescNode.get("mrp").asText();

			otbQuery = "select balance from " + database + ".BALANCE_OTB where invarticle_code = '" + articleCode + "' "
					+ "and desc6  = '" + desc6 + "' and month_year = '" + monthYear + "'";

//			System.out.println("otbquery :"+otbQuery);

			otbRS = stmt.executeQuery(otbQuery);

			if (otbRS != null && otbRS.next()) {
				catDescNode.put("otb", otbRS.getString("balance"));
				otbRS.close();
			} else {
				catDescNode.put("otb", "0");
			}

			for(int i = 0; i < itemParameters.length; i++) {

				itemListQuery = " select distinct  item_id, ccode5, ccode6, cname5, cname6,  smudfstrin01, smudfstrin02, smudfstrin03, smudfstrin04, smudfstrin05,\n"  
						+ " smudfstrin06, smudfstrin07, smudfstrin08, smudfstrin09, smudfstrin010, smudfnum01, smudfnum02, smudfnum03, smudfnum04, smudfnum05, \n"  
						+ " smudfdate01, smudfdate02, smudfdate03, smudfdate04, smudfdate05 from (select distinct order_date, item_id, ccode5, ccode6, cname5, cname6, 'S' smudfstrin01, smudfstrin02, smudfstrin03, smudfstrin04, smudfstrin05,\n"
						+ " smudfstrin06, smudfstrin07, smudfstrin08, smudfstrin09, smudfstrin010, smudfnum01, smudfnum02, smudfnum03, smudfnum04, smudfnum05, \n"
						+ " smudfdate01, smudfdate02, smudfdate03, smudfdate04, smudfdate05 from " + database
						+ ".gin_purord where item_id = " + itemParameters[i].replace("\"", "'") + " and cname5 is not null and cname6 is not null \n"
						+ " ORDER BY order_date DESC ) where rownum = 1 ";

				itemDetailRS = stmt.executeQuery(itemListQuery);

				while (itemDetailRS != null && itemDetailRS.next()) {
					itemsNode = mapper.createObjectNode();

					itemsNode.put("itemId",
							(itemDetailRS.getString("item_id") != null) ? itemDetailRS.getString("item_id") : "");
					itemsNode.put("cat5Code",
							(itemDetailRS.getString("ccode5") != null) ? itemDetailRS.getString("ccode5") : "");
					itemsNode.put("cat5Name",
							(itemDetailRS.getString("cname5") != null) ? itemDetailRS.getString("cname5") : "");
					itemsNode.put("cat6Code",
							(itemDetailRS.getString("ccode6") != null) ? itemDetailRS.getString("ccode6") : "");
					itemsNode.put("cat6Name",
							(itemDetailRS.getString("cname6") != null) ? itemDetailRS.getString("cname6") : "");
					itemsNode.put("udf1",
							(itemDetailRS.getString("smudfstrin01") != null) ? itemDetailRS.getString("smudfstrin01") : "");
					itemsNode.put("udf2",
							(itemDetailRS.getString("smudfstrin02") != null) ? itemDetailRS.getString("smudfstrin02") : "");
					itemsNode.put("udf3",
							(itemDetailRS.getString("smudfstrin03") != null) ? itemDetailRS.getString("smudfstrin03") : "");
					itemsNode.put("udf4",
							(itemDetailRS.getString("smudfstrin04") != null) ? itemDetailRS.getString("smudfstrin04") : "");
					itemsNode.put("udf5",
							(itemDetailRS.getString("smudfstrin05") != null) ? itemDetailRS.getString("smudfstrin05") : "");
					itemsNode.put("udf6",
							(itemDetailRS.getString("smudfstrin06") != null) ? itemDetailRS.getString("smudfstrin06") : "");
					itemsNode.put("udf7",
							(itemDetailRS.getString("smudfstrin07") != null) ? itemDetailRS.getString("smudfstrin07") : "");
					itemsNode.put("udf8",
							(itemDetailRS.getString("smudfstrin08") != null) ? itemDetailRS.getString("smudfstrin08") : "");
					itemsNode.put("udf9",
							(itemDetailRS.getString("smudfstrin09") != null) ? itemDetailRS.getString("smudfstrin09") : "");
					itemsNode.put("udf10",
							(itemDetailRS.getString("smudfstrin010") != null) ? itemDetailRS.getString("smudfstrin010")
									: "");
					itemsNode.put("udf11",
							(itemDetailRS.getString("smudfnum01") != null) ? itemDetailRS.getString("smudfnum01") : "");
					itemsNode.put("udf12",
							(itemDetailRS.getString("smudfnum02") != null) ? itemDetailRS.getString("smudfnum02") : "");
					itemsNode.put("udf13",
							(itemDetailRS.getString("smudfnum03") != null) ? itemDetailRS.getString("smudfnum03") : "");
					itemsNode.put("udf14",
							(itemDetailRS.getString("smudfnum04") != null) ? itemDetailRS.getString("smudfnum04") : "");
					itemsNode.put("udf15",
							(itemDetailRS.getString("smudfnum05") != null) ? itemDetailRS.getString("smudfnum05") : "");
					itemsNode.put("udf16",
							(itemDetailRS.getString("smudfdate01") != null) ? itemDetailRS.getString("smudfdate01") : "");
					itemsNode.put("udf17",
							(itemDetailRS.getString("smudfdate02") != null) ? itemDetailRS.getString("smudfdate02") : "");
					itemsNode.put("udf18",
							(itemDetailRS.getString("smudfdate03") != null) ? itemDetailRS.getString("smudfdate03") : "");
					itemsNode.put("udf19",
							(itemDetailRS.getString("smudfdate04") != null) ? itemDetailRS.getString("smudfdate04") : "");
					itemsNode.put("udf20",
							(itemDetailRS.getString("smudfdate05") != null) ? itemDetailRS.getString("smudfdate05") : "");
					itemDetailNode.add(itemsNode);
				}
				
			}
			
			

			if (itemsNode != null) {
				items.put("catdescudf", catDescNode);
				items.put("itemDetail", itemDetailNode);
			}
		} else {
			catDescNode = null;
		}
		itemDetailRS.close();

		con.close();

		return items;
	}

	public static int[] createFMCGPO(List<GinesysPurchaseOrder> gpo)
			throws SQLException, ClassNotFoundException, IOException {

		List<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
		final int batchSize = 1000;
		int count = 0;
		Properties ginesysProperties = ConnectionUtils.getProperties("resources/ginesys.properties");
		String database = ginesysProperties.getProperty("databaseqa");
		Connection con = ConnectionUtils.getConnection(ginesysProperties);

		int[] result= new int[gpo.size()];
		
		String sql = "INSERT INTO " + database + ".FMCG_PURORD_QA(INTGCODE, INTG_HEADER_ID, INTG_LINE_ID,"
				+ "				ORDER_NO, ORDER_DATE, VENDOR_ID, TRANSPORTER_ID, AGENT_ID, AGENT_RATE,"
				+ "				PO_REMARKS, CREATED_BY_ID, VALID_FROM, VALID_TO, MERCHANDISER_ID,"
				+ "				SITE_ID, ITEM_ID, SET_REMARKS, SET_RATIO, ARTICLE_ID, ITEM_NAME,"
				+ "				CCODE1, CCODE2, CCODE3, CCODE4, CCODE5, CCODE6, CNAME1, CNAME2,"
				+ "				CNAME3, CNAME4, CNAME5, CNAME6, DESC1, DESC2, DESC3, DESC4, DESC5,"
				+ "				DESC6, MRP, LISTED_MRP, WSP, UOM, MATERIAL_TYPE, QTY, RATE,"
				+ "				PO_ITEM_REMARKS, ISIMPORTED, TERMCODE, ISVALIDATED, VALIDATION_ERROR,"
				+ "				DOCCODE, SET_HEADER_ID, KEY, POUDFSTRIN01, POUDFSTRIN02, POUDFSTRIN03,"
				+ "				POUDFSTRIN04, POUDFSTRIN05, POUDFSTRIN06, POUDFSTRIN07, POUDFSTRIN08,"
				+ "				POUDFSTRIN09, POUDFSTRIN010, POUDFNUM01, POUDFNUM02, POUDFNUM03,"
				+ "				POUDFNUM04, POUDFNUM05, POUDFDATE01, POUDFDATE02, POUDFDATE03,"
				+ "				POUDFDATE04, POUDFDATE05, SMUDFSTRIN01, SMUDFSTRIN02, SMUDFSTRIN03,"
				+ "				SMUDFSTRIN04, SMUDFSTRIN05, SMUDFSTRIN06, SMUDFSTRIN07, SMUDFSTRIN08,"
				+ "				SMUDFSTRIN09, SMUDFSTRIN010, SMUDFNUM01, SMUDFNUM02, SMUDFNUM03,"
				+ "				SMUDFNUM04, SMUDFNUM05, SMUDFDATE01, SMUDFDATE02, SMUDFDATE03,"
				+ "				SMUDFDATE04, SMUDFDATE05, IMUDFSTRING01, IMUDFSTRING02, IMUDFSTRING03,"
				+ "				IMUDFSTRING04, IMUDFSTRING05, IMUDFSTRING06, IMUDFSTRING07,"
				+ "				IMUDFSTRING08, IMUDFSTRING09, IMUDFSTRING10, IMUDFNUM01, IMUDFNUM02,"
				+ "				IMUDFNUM03, IMUDFNUM04, IMUDFNUM05, IMUDFDATE01, IMUDFDATE02,"
				+ "				IMUDFDATE03, IMUDFDATE04, IMUDFDATE05,PO_REFERENCE_ID, HSN_CODE ) VALUES "
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement pstm = con.prepareStatement(sql);

		for (GinesysPurchaseOrder po : gpo) {
			pstm.setString(1, po.getIntGCode());
			pstm.setString(2, po.getIntGHeaderId());
			pstm.setString(3, po.getIntGLineId());
			pstm.setString(4, po.getOrderNo());
			pstm.setTimestamp(5, po.getOrderDate());
			pstm.setString(6, po.getVendorId());
			pstm.setString(7, po.getTransporterId());
			pstm.setString(8, po.getAgentId());
			pstm.setDouble(9, po.getAgentRate());
			pstm.setString(10, po.getPoRemarks());
			pstm.setString(11, po.getCreatedById());
			pstm.setTimestamp(12, po.getValidFrom());
//		System.out.println("Valid from :" + po.getValidFrom());
			pstm.setTimestamp(13, po.getValidTo());
			pstm.setString(14, po.getMerchandiserId());
			pstm.setString(15, po.getSiteId());
			pstm.setString(16, po.getItemId());
			pstm.setString(17, po.getSetRemarks());
			pstm.setDouble(18, po.getSetRatio());
			pstm.setString(19, po.getArticleId());
			pstm.setString(20, po.getItemName());
			pstm.setString(21, po.getcCode1());
			pstm.setString(22, po.getcCode2());
			pstm.setString(23, po.getcCode3());
			pstm.setString(24, po.getcCode4());
			pstm.setString(25, po.getcCode5());
			pstm.setString(26, po.getcCode6());
			pstm.setString(27, po.getcName1());
			pstm.setString(28, po.getcName2());
			pstm.setString(29, po.getcName3());
			pstm.setString(30, po.getcName4());
			pstm.setString(31, po.getcName5());
			pstm.setString(32, po.getcName6());
			pstm.setString(33, po.getDesc1());
			pstm.setString(34, po.getDesc2());
			pstm.setString(35, po.getDesc3());
			pstm.setString(36, po.getDesc4());
			pstm.setString(37, po.getDesc5());
			pstm.setString(38, po.getDesc6());
			pstm.setDouble(39, po.getMrp());
			pstm.setDouble(40, po.getListedMRP());
			pstm.setDouble(41, po.getWsp());
			pstm.setString(42, po.getUom());
			pstm.setString(43, String.valueOf(po.getMaterialType()));
			pstm.setDouble(44, po.getQty());
			pstm.setDouble(45, po.getRate());
			pstm.setString(46, po.getPoItemRemarks());
			pstm.setString(47, String.valueOf(po.getIsImported()));
			pstm.setString(48, po.getTermCode());
			pstm.setString(49, String.valueOf(po.getIsValidated()));
			pstm.setString(50, po.getValidationError());
			pstm.setString(51, po.getDocCode());
			pstm.setString(52, po.getSetHeaderId());
			pstm.setInt(53, po.getKey());
			pstm.setString(54, po.getPoudfStrin01());
			pstm.setString(55, po.getPoudfStrin02());
			pstm.setString(56, po.getPoudfStrin03());
			pstm.setString(57, po.getPoudfStrin04());
			pstm.setString(58, po.getPoudfStrin05());
			pstm.setString(59, po.getPoudfStrin06());
			pstm.setString(60, po.getPoudfStrin07());
			pstm.setString(61, po.getPoudfStrin08());
			pstm.setString(62, po.getPoudfStrin09());
			pstm.setString(63, po.getPoudfStrin010());
			pstm.setString(64, po.getPoudfNum01());
			pstm.setString(65, po.getPoudfNum02());
			pstm.setString(66, po.getPoudfNum03());
			pstm.setString(67, po.getPoudfNum04());
			pstm.setString(68, po.getPoudfNum05());
			pstm.setTimestamp(69, po.getPoudfDate01());
			pstm.setTimestamp(70, po.getPoudfDate02());
			pstm.setTimestamp(71, po.getPoudfDate03());
			pstm.setTimestamp(72, po.getPoudfDate04());
			pstm.setTimestamp(73, po.getPoudfDate05());
			pstm.setString(74, po.getSmUDFStrin01());
			pstm.setString(75, po.getSmUDFStrin02());
			pstm.setString(76, po.getSmUDFStrin03());
			pstm.setString(77, po.getSmUDFStrin04());
			pstm.setString(78, po.getSmUDFStrin05());
			pstm.setString(79, po.getSmUDFStrin06());
			pstm.setString(80, po.getSmUDFStrin07());
			pstm.setString(81, po.getSmUDFStrin08());
			pstm.setString(82, po.getSmUDFStrin09());
			pstm.setString(83, po.getSmUDFStrin010());
			pstm.setString(84, po.getSmUDFNum01());
			pstm.setString(85, po.getSmUDFNum02());
			pstm.setString(86, po.getSmUDFNum03());
			pstm.setString(87, po.getSmUDFNum04());
			pstm.setString(88, po.getSmUDFNum05());
			pstm.setTimestamp(89, po.getSmUDFDate01());
			pstm.setTimestamp(90, po.getSmUDFDate02());
			pstm.setTimestamp(91, po.getSmUDFDate03());
			pstm.setTimestamp(92, po.getSmUDFDate04());
			pstm.setTimestamp(93, po.getSmUDFDate05());
			pstm.setString(94, po.getImUDFString01());
			pstm.setString(95, po.getImUDFString02());
			pstm.setString(96, po.getImUDFString03());
			pstm.setString(97, po.getImUDFString04());
			pstm.setString(98, po.getImUDFString05());
			pstm.setString(99, po.getImUDFString06());
			pstm.setString(100, po.getImUDFString07());
			pstm.setString(101, po.getImUDFString08());
			pstm.setString(102, po.getImUDFString09());
			pstm.setString(103, po.getImUDFString010());
			pstm.setString(104, po.getImUDFNum01());
			pstm.setString(105, po.getImUDFNum02());
			pstm.setString(106, po.getImUDFNum03());
			pstm.setString(107, po.getImUDFNum04());
			pstm.setString(108, po.getImUDFNum05());
			pstm.setTimestamp(109, po.getImUDFDate01());
			pstm.setTimestamp(110, po.getImUDFDate02());
			pstm.setTimestamp(111, po.getImUDFDate03());
			pstm.setTimestamp(112, po.getImUDFDate04());
			pstm.setTimestamp(113, po.getImUDFDate05());
			pstm.setString(114, po.getPoReferenceId());
			pstm.setString(115, po.getHsnCode());

			pstm.addBatch();

			if (++count % batchSize == 0) {
				result = pstm.executeBatch();
				pstm.clearBatch();
			}
		}

		result = pstm.executeBatch();

		con.close();

		return result;
	}
}
