package smpartner.util;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class TimestampDeserializer extends JsonDeserializer<java.sql.Timestamp> {

	@Override
	public Timestamp deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
		String str = p.getText();
		try {
	         DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
	         
	         OffsetDateTime offsetDateTime = OffsetDateTime.parse(str, dateTimeFormatter);
	         Timestamp timestampDate = Timestamp.valueOf(offsetDateTime.toLocalDateTime());
			 return timestampDate;
		} catch (DateTimeParseException e) {
//			System.err.println(e);
			return null;
		}catch(Exception ex){
			return null;
		}
	}
}