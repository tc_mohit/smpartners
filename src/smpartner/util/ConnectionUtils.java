package smpartner.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtils {

	public static Connection getConnection(Properties ginesysProperties) throws ClassNotFoundException, SQLException, IOException {

//		Properties ginesysProperties = new Properties();
//		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
//		InputStream ginesysStream = classloader.getResourceAsStream("ginesys.properties");
//		ginesysProperties.load(ginesysStream);

//		Properties ginesysProperties = getProperties("resources/ginesys.properties");
		String connectionURL = ginesysProperties.getProperty("url");
		String driverClass= ginesysProperties.getProperty("driverclass");
		String userName = ginesysProperties.getProperty("username");
		String password = ginesysProperties.getProperty("password");

		return getConnection(connectionURL, driverClass, userName, password);
	}

	public static Connection getConnection(String connectionURL, String driverClass, String userName, String password)
			throws ClassNotFoundException, SQLException {
		Class.forName(driverClass);
		Connection conn = DriverManager.getConnection(connectionURL, userName, password);
		return conn;
	}
	
	public static Properties getProperties(String filename) throws IOException{
		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream ginesysStream = classloader.getResourceAsStream(filename);
		properties.load(ginesysStream);
		return properties;
	}
}
