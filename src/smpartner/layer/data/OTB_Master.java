package smpartner.layer.data;

import java.sql.Timestamp;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import smpartner.util.TimestampDeserializer;

public class OTB_Master {

	@JsonDeserialize(using = TimestampDeserializer.class)
//	@JsonSerialize(using = TimestampSerializer.class)
	private Timestamp validTo;
	
	private int articleCode;
	private String desc6;
	public OTB_Master() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OTB_Master(Timestamp validTo, int articleCode, String desc6) {
		super();
		this.validTo = validTo;
		this.articleCode = articleCode;
		this.desc6 = desc6;
	}
	public Timestamp getValidTo() {
		return validTo;
	}
	public void setValidTo(Timestamp validTo) {
		this.validTo = validTo;
	}
	public int getArticleCode() {
		return articleCode;
	}
	public void setArticleCode(int articleCode) {
		this.articleCode = articleCode;
	}
	public String getDesc6() {
		return desc6;
	}
	public void setDesc6(String desc6) {
		this.desc6 = desc6;
	}
	@Override
	public String toString() {
		return "OTB_Master [validTo=" + validTo + ", articleCode=" + articleCode + ", desc6=" + desc6 + "]";
	}
	
	
	
	
	
}
