package smpartner.layer.data;

public class Employee {

	private String name;
	private String rollNo;
	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Employee(String name, String rollNo) {
		super();
		this.name = name;
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRollNo() {
		return rollNo;
	}
	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", rollNo=" + rollNo + "]";
	}
	
	
	
}
