package smpartner.layer.web;


import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import smpartner.exception.StatusCodes.AppStatusCodes;
import smpartner.layer.web.model.AppResponse;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbstractEndpoint {

	protected static ObjectMapper getMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		return mapper;
	}

	protected static ArrayNode getArrayNode() {
		return getMapper().createArrayNode();
	}

	protected static ObjectNode getObjectNode() {
		return getMapper().createObjectNode();
	}


	AppResponse buildOkResponse() {
		return new AppResponse.Builder().status(AppStatusCodes.genericSuccessCode).build();
	}
	
	protected AppResponse buildErrorResponse(String statusCode, String errorCode, String errorMessage) {

		ObjectNode error = getMapper().createObjectNode();
		error.put("errorCode", errorCode).put("errorMessage", errorMessage);
		return new AppResponse.Builder().status(statusCode).error(error).build();
	}

}
