package smpartner.layer.web.endpoint;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import smpartner.exception.StatusCodes.AppModuleCode;
import smpartner.exception.StatusCodes.AppModuleMsg;
import smpartner.exception.StatusCodes.AppStatusCodes;
import smpartner.exception.StatusCodes.AppStatusMsg;
import smpartner.exception.StatusCodes.ClientErrors;
import smpartner.exception.StatusCodes.ServerErrors;
import smpartner.layer.data.Employee;
import smpartner.layer.data.GinesysPurchaseOrder;
import smpartner.layer.web.AbstractEndpoint;
import smpartner.layer.web.model.AppResponse;
import smpartner.util.CodeUtils;
import smpartner.util.DBUtils;

//@Path("/ginesys")
public class GinesysEndpoint extends AbstractEndpoint {

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse createPO(JsonNode data) {
		AppResponse appResponse = null;
		String queriesResult = "";
		GinesysPurchaseOrder po = null;
		try {

			if (data != null) {

				JsonNode poJsonList = data.get("poData");

				for (JsonNode poJson : poJsonList) {
					po = getMapper().convertValue(poJson, GinesysPurchaseOrder.class);

					if (po != null) {
						int insertResult = DBUtils.insertPurchaseOrder(po);
						queriesResult = queriesResult + insertResult + ",";
					} else {
						queriesResult = queriesResult + "0,";
					}
				}
				if (!queriesResult.contains("0") && queriesResult != "") {

					System.out.print("Ginesys PO created :" + po.getOrderNo());

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					System.out.print("Ginesys PO not created :" + po.getOrderNo());

					appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
							AppModuleCode.POCREATORERROR, AppModuleMsg.PO_ERROR_MSG);
				}
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}

	@POST
	@Path("/get/otb")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getOTBByProcedure(JsonNode data) {
		AppResponse appResponse = null;
		Map<String, String> otbMap = null;

		Map<String, String> otbRequest = new HashMap<String, String>();
		try {

			if (data != null) {
				String articleCode = data.get("articleCode").asText();
				String desc6 = data.get("desc6").asText();
				String monthYear = data.get("monthYear").asText();

				otbRequest.put("articleCode", articleCode);
				otbRequest.put("desc6", desc6);
				otbRequest.put("monthYear", monthYear);
				otbMap = DBUtils.getOTB(otbRequest);

				if (otbMap != null) {
					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).put("otb", otbMap.get("otb")))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
							AppModuleCode.POOTB_READER_ERROR, AppModuleMsg.PO_OTB_ERROR_MSG);
				}
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (SQLException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (ClassNotFoundException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}

	@POST
	@Path("/get/hsn")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getHSNByDepartment(JsonNode data) {
		AppResponse appResponse = null;
		Map<String, String> hsnMap = null;
		try {

			if (data != null) {
				String hl3Code = data.get("hl3Code").asText();
				hsnMap = DBUtils.getHsnByHl3Code(hl3Code);

				if (hsnMap != null) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)
									.put("hsnCode", hsnMap.get("hsnCode")).put("hsnSacCode", hsnMap.get("hsnSacCode")))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
							AppModuleCode.PO_HSN_READER_ERROR, AppModuleMsg.PO_HSN_ERROR_MSG);
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
						AppModuleCode.PO_HSN_READER_ERROR, "data not found");

			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (SQLException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (ClassNotFoundException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}

	@POST
	@Path("/get/item/barcode")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getItemBarcode(JsonNode data) {
		AppResponse appResponse = null;
		List<Map<String, String>> itemList = null;
		ArrayNode arrNode = getMapper().createArrayNode();
		ObjectNode node = getMapper().createObjectNode();

		Map<String, String> itemRequest = new HashMap<String, String>();
		try {

			if (data != null) {

				itemRequest.put("siteId", data.get("siteId").asText());
				itemRequest.put("articleCode", data.get("articleCode").asText());
				itemRequest.put("supplierCode", data.get("supplierCode").asText());
				itemRequest.put("type", data.get("type").asText());
				itemRequest.put("search", data.get("search").asText());
				itemRequest.put("pageNo", data.get("pageNo").asText());

				itemList = DBUtils.getItemBarcode(itemRequest);

				if (!CodeUtils.isEmpty(itemList) && itemList.size() > 1) {
					for(int i = 0; i< itemList.size();i++){
						if (i > 0) {
							ObjectNode tempItemNode = getMapper().createObjectNode();
							tempItemNode.put("itemId", itemList.get(i).get("item_id"));
							arrNode.add(tempItemNode);
						}
					}

					node.put(CodeUtils.RESPONSE, arrNode);

					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)
									.put("currPage", itemList.get(0).get("currPage"))
									.put("prePage", itemList.get(0).get("prePage"))
									.put("maxPage", itemList.get(0).get("maxPage")).putAll(node))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.genericSuccessCode).build();

				}
			}
		} catch (JsonMappingException jsonException) {
			System.out.println(jsonException.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			System.out.println(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}

	@POST
	@Path("/get/item/barcode/detail")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getItemBarcodeDetail(JsonNode data) {
		AppResponse appResponse = null;
		ObjectNode itemNode = getMapper().createObjectNode();
		ObjectNode node = getMapper().createObjectNode();

		try {
			if (data != null) {
				
				itemNode = DBUtils.getItemBarcodeDetail(data);
				if (itemNode != null) {
					node.putPOJO(CodeUtils.RESPONSE, itemNode);

					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.genericSuccessCode).build();

				}
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (SQLException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (ClassNotFoundException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}

	
	@POST
	@Path("fmcg/create/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse saveFMCGPO(JsonNode fmcgData) {
		AppResponse appResponse = null;
		ObjectNode node = getMapper().createObjectNode();
		ArrayNode arrNode = getMapper().createArrayNode();

		try {
			if (fmcgData != null) {
				
				System.out.println("fmcg creation");
				
				arrNode = (ArrayNode) fmcgData.get("fmcgData");
				String fmcgStr = arrNode.toString();
				
				List<GinesysPurchaseOrder> gpoList = getMapper().readValue(fmcgStr, 
						getMapper().getTypeFactory().constructCollectionType(List.class, GinesysPurchaseOrder.class));
				
				int[] result = DBUtils.createFMCGPO(gpoList);
				if (!Arrays.toString(result).contains("0")) {
					node.putPOJO(CodeUtils.RESPONSE, "Records created successfully");

					appResponse = new AppResponse.Builder().data(getMapper().createObjectNode()
							.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG).putAll(node))
							.status(AppStatusCodes.genericSuccessCode).build();
				} else {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.BLANK))
							.status(AppStatusCodes.genericSuccessCode).build();

				}
			}else {
				appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
						AppStatusMsg.BLANK_JSON);
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (SQLException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (ClassNotFoundException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
		} finally {
			return appResponse;
		}
	}
	
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/purchaseorder/get")
	public String sayHtmlHello() {
		return "Hello from Jersey 2";
	}

	@GET
	@Path("/getjson")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTrackInJSON() {

		String result = "Track saved : ";
		return Response.status(201).entity(result).build();

	}

	@POST
	@Path("/testjson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createEmployeeJSON(JsonNode data) {

		JsonNode poJson = data.get("poData");

		Employee em = getMapper().convertValue(poJson, Employee.class);

		String result = "Track saved : " + em;
		return Response.status(201).entity(result).build();

	}
}
