package smpartner.layer.web.endpoint;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import smpartner.exception.StatusCodes.AppModuleCode;
import smpartner.exception.StatusCodes.AppModuleMsg;
import smpartner.exception.StatusCodes.AppStatusCodes;
import smpartner.exception.StatusCodes.AppStatusMsg;
import smpartner.exception.StatusCodes.ClientErrors;
import smpartner.exception.StatusCodes.ServerErrors;
import smpartner.layer.data.GinesysPurchaseOrder;
import smpartner.layer.web.AbstractEndpoint;
import smpartner.layer.web.model.AppResponse;
import smpartner.util.CodeUtils;
import smpartner.util.ConnectionUtils;
import smpartner.util.DBUtils;

//@Path("/ginesys/jcb")
public class JcbEndpoint extends AbstractEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(JcbEndpoint.class);
	private String entPrefix = "jcb";

	@SuppressWarnings("finally")
	@POST
	@Path("/po/create")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse createPO(JsonNode data) {
		AppResponse appResponse = null;
		String queriesResult = "";
		try {

			Properties properties = CodeUtils.setAllPropertiesAccordingEnterprise(entPrefix);
			LOGGER.debug(
					"Got all the properties according to respective enterprise->" + properties.getProperty("database"));
			if (data != null) {

				JsonNode poJsonList = data.get("poData");
				LOGGER.debug("Get the value from 'poData' in JsonNode" + poJsonList);

				for (JsonNode poJson : poJsonList) {
					GinesysPurchaseOrder po = getMapper().convertValue(poJson, GinesysPurchaseOrder.class);
					LOGGER.debug("Converted the Json in 'GinesysPurchaseOrder' POJO" + po);

					if (po != null) {
//						int insertResult = DBUtils.insertJcbPO(po);
						int insertResult = DBUtils.insertPOGinesys(po, properties);
						queriesResult = queriesResult + insertResult + ",";
						LOGGER.debug("PO inserted in SMPartner-> " + "insertResult = " + insertResult
								+ "|| queriesResult = " + queriesResult);
					} else {
						queriesResult = queriesResult + "0,";
						LOGGER.debug("queriesResult = " + queriesResult);
					}
				}
				if (!queriesResult.contains("0") && queriesResult != "") {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().putPOJO(CodeUtils.RESPONSE, null)
									.put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG))
							.status(AppStatusCodes.genericSuccessCode).build();
					LOGGER.debug("SUCCESS RESPONSE-> " + appResponse);
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
							AppModuleCode.POCREATORERROR, AppModuleMsg.PO_ERROR_MSG);
					LOGGER.debug("ERROR RESPONSE-> " + appResponse);
				}
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());
			LOGGER.error(jsonException.getMessage(), jsonException);
		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
			LOGGER.error(e.getMessage(), e);
		} finally {
			return appResponse;
		}
	}

	@SuppressWarnings("finally")
	@POST
	@Path("/get/hsn")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public AppResponse getHSNByDepartment(JsonNode data) {
		AppResponse appResponse = null;
		Map<String, String> hsnMap = null;
		try {
//			Properties properties = ConnectionUtils.getProperties("resources/jcb.properties");

			Properties properties = CodeUtils.setAllPropertiesAccordingEnterprise(entPrefix);
			LOGGER.debug(
					"Got all the properties according to respective enterprise->" + properties.getProperty("database"));
			if (data != null) {
				String hl3Code = data.get("hl3Code").asText();
				hsnMap = DBUtils.getHsnByHl3CodeGenerics(properties, hl3Code);
				LOGGER.debug("Got the HSN data according to HL3CODE(Department code)" + hsnMap);
				if (hsnMap != null) {
					appResponse = new AppResponse.Builder()
							.data(getMapper().createObjectNode().put(CodeUtils.RESPONSE_MSG, AppStatusMsg.SUCCESS_MSG)
									.put("hsnCode", hsnMap.get("hsnCode")).put("hsnSacCode", hsnMap.get("hsnSacCode")))
							.status(AppStatusCodes.genericSuccessCode).build();
					LOGGER.debug("SUCCESS RESPONSE-> " + appResponse);
				} else {
					appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
							AppModuleCode.PO_HSN_READER_ERROR, AppModuleMsg.PO_HSN_ERROR_MSG);
					LOGGER.debug("ERROR RESPONSE-> " + appResponse);
				}
			} else {
				appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode,
						AppModuleCode.PO_HSN_READER_ERROR, "data not found");
				LOGGER.debug("DATA NOT FOUND RESPONSE-> " + appResponse);
			}
		} catch (JsonMappingException jsonException) {
			appResponse = buildErrorResponse(AppStatusCodes.genericClientErrorCode, ClientErrors.badRequest,
					jsonException.getMessage());
			LOGGER.error(jsonException.getMessage(), jsonException);

		} catch (SQLRecoverableException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (SQLException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (ClassNotFoundException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (IOException ex) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (Exception e) {
			appResponse = buildErrorResponse(AppStatusCodes.genericServerErrorCode, ServerErrors.internalServerError,
					e.getMessage());
			LOGGER.error(e.getMessage(), e);
		} finally {
			return appResponse;
		}
	}

	@GET
	@Path("/getjson")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTrackInJSON() {

		String result = "Track saved : ";
		LOGGER.debug("Logger printed" + result);
		return Response.status(201).entity(result).build();

	}
}
